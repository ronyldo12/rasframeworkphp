<?php

namespace RAS;

class Router {

    private $routers;
    private $controller;
    private $action;
    private $defaultController;
    private $defaultAction;
    private $module;
    private $controllerValido;
    private $actionValida;

    public function __construct() {
        $this->routers = array();
        $this->defaultController = 'indexController';
        $this->defaultAction = 'index';
        $this->controllerValido = false;
        $this->actionValida = false;
    }

    public function set($rota, $class) {
        $this->routers[] = array($rota, $class);
    }

    public function setDefaultController($controller) {
        $this->defaultController = $controller;
    }

    public function getRouters() {
        return $this->routers;
    }

    public function getController() {
        return $this->controllerValido;
    }

    public function getAction() {
        return $this->actionValida;
    }

    public function carregaControllerAndAction() {
        $config = new \RAS\Config();
        $url = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
        $uri = rtrim(ltrim($url, '/'), '/');

        $config->load('router');

       
        foreach ($config->get_config() as $route => $routeInfo) {
            $route = rtrim(ltrim($route, '/'), '/');
            if ($uri == $route) {
                $this->module = $routeInfo[0];
                $this->controller = $routeInfo[1];
                $this->action = $routeInfo[2];
            }
        }

        
        if ($this->controller == false) {
            if ($uri != '') {
                $arrayUri = @explode('/', $uri);
                $this->controller = isset($arrayUri[0]) ? $arrayUri[0] . 'Controller' : $this->defaultController;
                $this->action = isset($arrayUri[1]) ? $arrayUri[1] : $this->defaultAction;
            } else {

                $this->controller = $this->defaultController;
                $this->action = $this->defaultAction;
            }
        }

        if($this->module==false){
        
        $config->load('modules');
        $modules = $config->item('modules');


        foreach ($modules as $module) {
            $eValido = $this->verificarUri($module);
            if ($eValido) {
                $this->module = $module;
                $this->controllerValido = "\\" . $this->module . "\\Controllers\\" . $this->controller;
                $this->actionValida = $this->action;
                break;
            }
        }
        
        }else{
            
            $eValido = $this->verificarUri($this->module);
            if ($eValido) {
                $this->controllerValido = "\\" . $this->module . "\\Controllers\\" . $this->controller;
                $this->actionValida = $this->action;
            }
            
        }

        if ($this->controllerValido == false) {
            throw new \Exception("Controller " . $this->controller . " não encontrado");
        }
        if ($this->actionValida == false) {
            throw new \Exception("Action " . $this->action . " não encontrada no controller " . $this->controller);
        }
    }

    public function verificarUri($module) {

        $namespaceController = '\\' . $module . '\\Controllers\\' . $this->controller;
        if (!class_exists($namespaceController)) {
            return false;
        }

        $controller = new $namespaceController();
        if (!method_exists($controller, $this->action)) {
            return false;
        }
        return true;
    }

}