<?php
require_once 'Log.php';
require_once 'Error.php';

error_reporting( E_ERROR );
set_error_handler("myerror");

$module = 'Produtos';
$route = new \RAS\Router();
$route->carregaControllerAndAction();

$namespace = $route->getController();
$actionComplete = $route->getAction();
$controller = new $namespace();
$controller->$actionComplete();



